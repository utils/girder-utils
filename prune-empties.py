#!/usr/bin/env python3

import argparse
import girder_client


def main(instance, key, folders, dry_run=False):
    client = girder_client.GirderClient(apiUrl=f'https://{instance}/api/v1/')
    client.authenticate(apiKey=key)

    for root_folder_id in folders:
        print(f'checking {root_folder_id}')
        folders = client.listFolder(root_folder_id)
        for folder in folders:
            folder_id = folder['_id']
            folder_name = folder['name']
            details = client.get(f'folder/{folder_id}/details')
            nitems = details['nItems']
            nfolders = details['nFolders']
            print(f'folder {folder_name} has {nitems} items and {nfolders} folders')
            if not nitems and not nfolders:
                if not dry_run:
                    print(f'deleting {folder_name}')
                    client.delete(f'folder/{folder_id}')
                else:
                    print(f'would delete {folder_name}')


def options():
    p = argparse.ArgumentParser(
        description='Prune empty folders on a Girder instance')
    p.add_argument('-k', '--key', type=str, required=True,
        help='API key for girder')
    p.add_argument('-g', '--girder', type=str, default='data.kitware.com',
        help='Girder instance to talk to')
    p.add_argument('-n', '--dry-run', default=False, action='store_true',
        help='only log actions that would be performed')
    p.add_argument('folders', metavar='FOLDER', type=str, nargs='+',
        help='folder IDs to prune')
    return p


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    main(opts.girder, opts.key, opts.folders, opts.dry_run)
